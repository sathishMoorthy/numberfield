
import './App.css';
import React, { useState } from 'react'
import NumberField from './core/components/NumberField';



export default function App(props) {
  const [number, setNumber] = useState(0);
  const[readOnly,setReadyOnly] = useState(true)
 //const[maxIntegersDigits,setMaxIntegersDigits] = useState();

  const onValueChanged = (newValue) => {
    setNumber(newValue);  
  }


  return (
    <div className="App">
      Label:< NumberField labelname="Number" />

      <br></br>
      Value:< NumberField value={number} onChange={onValueChanged} />

      <br></br>
      Placeholder:<NumberField placeholder="Enter Number" />

      <br></br>
      Tooltip:<NumberField tooltip="Must be Numbers" />

      <br></br>
      Disabled:<NumberField disabled={true}   />

      <br></br>
     Editable:<NumberField read={readOnly} value={number} onChange={onValueChanged}/>

    

    </div>
  );
}


