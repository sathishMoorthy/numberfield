
import React from 'react';


const NumberField = (props) => {

  const onNumberChange = (e) => {
    let newValue = e.target.value;

    props.onChange(newValue);
  }

  // const onMaxNumberChanged = (e) => {
  //   let newMaxValue = e.target.value;
  //   if (newMaxValue.length <=10) {
  //     props.onInput(newMaxValue);

  //   }

 // }

  return (
    <div> 

      {props.labelname}
      <input type='number' className='input' disabled={props.disabled} placeholder={props.placeholder}
        title={props.tooltip} value={props.value} onChange={onNumberChange} readOnly={props.read} ></input>


    </div>

  )
}

export default NumberField;
